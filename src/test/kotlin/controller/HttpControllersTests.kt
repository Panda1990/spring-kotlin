package controller

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import mario.personal.project.SpringKotlin.persistence.Article
import mario.personal.project.SpringKotlin.persistence.User
import mario.personal.project.SpringKotlin.repositories.ArticleRepository
import mario.personal.project.SpringKotlin.repositories.UserRepository
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import kotlin.coroutines.EmptyCoroutineContext

class HttpControllersTests (@Autowired val mockMvc: MockMvc) {
    @MockkBean
    private lateinit var userRepository: UserRepository

    @MockkBean
    private lateinit var articleRepository: ArticleRepository

    @Test
    fun `List articles` () {
        val juergen = User ("springjuergen", "Juergen", "Hoeller")
        val spring5Article = Article ("Spring Framework 5.0 goes GA", "Dear Spring community ...", "Lorem ipsum", juergen)
        val spring4Article = Article ("Spring Framework 4.3 goes GA", "Dear Spring community ...", "Lorem ipsum", juergen)
        every { articleRepository.findAllByOrderByAddedAtDesc() } returns listOf(spring5Article, spring4Article)
        every { articleRepository.findAllByOrderByAddedAtDesc() } returns listOf(spring5Article, spring4Article)
       /* mockMvc.perform(get("/api/article/").accept(MediaType.APPLICATION_JSON))
                .andExpect(ResponseEntity.status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("\$.[0].author.login").value(juergen.login))
                .andExpect(jsonPath("\$.[0].slug").value(spring5Article.slug))
                .andExpect(jsonPath("\$.[1].author.login").value(juergen.login))
                .andExpect(jsonPath("\$.[1].slug").value(spring43Article.slug))*/
    }

    @Test
    fun `List users`() {
        val juergen = User("springjuergen", "Juergen", "Hoeller")
        val smaldini = User("smaldini", "Stéphane", "Maldini")
        every { userRepository.findAll() } returns listOf(juergen, smaldini)
        /*mockMvc.perform(get("/api/user/").accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.jsonPath("\$.[0].login").value(juergen.login))
                .andExpect(MockMvcResultMatchers.jsonPath("\$.[1].login").value(smaldini.login))
    */}
}