var UserManager = {
    baseUrl: "http://localhost:8080",
    init: function (){
        document.getElementById('show-user').addEventListener("click", function(){
            console.log ("Start request");
            UserManager.getAllUser();
        });

        document.getElementById('search-user').addEventListener("click", function(){
                    console.log ("Start request");
                    UserManager.searchUser();
                });
    },
    getAllUser: function (){
        return jQuery.ajax({
            type: "GET",
                url: UserManager.baseUrl + "/api/user",
                beforeSend: function (request) {
                },
                contentType: "application/json",
                success: function (data, textStatus, jQxhr) {
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    var error =  JSON.parse(jqXhr.responseText);
                }
            });
        },

    searchUser: function (firstName, lastName){
        return jQuery.ajax({
            type: "GET",
                url: UserManager.baseUrl + "/api/user/"+firstName+"/"+lastName,
                beforeSend: function (request) {
                },
                contentType: "application/json",
                success: function (data, textStatus, jQxhr) {
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    var error =  JSON.parse(jqXhr.responseText);
                }
            });
        },
}