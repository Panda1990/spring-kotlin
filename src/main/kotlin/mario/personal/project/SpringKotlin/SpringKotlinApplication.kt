package mario.personal.project.SpringKotlin

import mario.personal.project.SpringKotlin.configuration.Properties
import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties (Properties::class)
class SpringKotlinApplication

fun main(args: Array<String>) {
	runApplication<SpringKotlinApplication>(*args){
		setBannerMode(Banner.Mode.OFF)
	}
}
