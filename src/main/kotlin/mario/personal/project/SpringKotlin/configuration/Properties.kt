package mario.personal.project.SpringKotlin.configuration

import org.springframework.boot.Banner
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties("blog")
class Properties (var title: String, val banner: Banner){
    data class Banner (val title: String? = null, val content: String)
}