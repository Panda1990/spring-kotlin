package mario.personal.project.SpringKotlin.controller

import mario.personal.project.SpringKotlin.repositories.ArticleRepository
import mario.personal.project.SpringKotlin.repositories.UserRepository
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/article")
class ArticleController (private val repository: ArticleRepository){
    @GetMapping ("/")
    fun findAll () = repository.findAllByOrderByAddedAtDesc();

    @GetMapping("/{slug}")
    fun findOne (@PathVariable slug: String) = repository.findBySlug(slug) ?: ResponseStatusException(HttpStatus.NOT_FOUND, "This article does not exists")
}

@RestController
@RequestMapping ("/api/user")
class UserController (private val repository: UserRepository){
    @GetMapping("/")
    fun findAll () = repository.findAll()

    @GetMapping("/{login}")
    fun findOne(@PathVariable login: String) = repository.findByLogin(login) ?: ResponseStatusException(HttpStatus.NOT_FOUND, "This user does not exists")

    @GetMapping("/{firstName}/{lastName}")
    fun findByNameandSurname (@PathVariable firstName: String, @PathVariable lastName: String) = repository.findByNameAndSurname (firstName, lastName) ?: ResponseStatusException(HttpStatus.NOT_FOUND, "User does not exists")
}