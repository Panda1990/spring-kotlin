package mario.personal.project.SpringKotlin.repositories.implementation

import mario.personal.project.SpringKotlin.persistence.User
import mario.personal.project.SpringKotlin.repositories.UserRepository
import mario.personal.project.SpringKotlin.repositories.service.UserService
import java.util.*


class UserRepositoryImpl (val userRepository: UserRepository) : UserService {
    override fun getUserByNameAndSurname(firstName: String, lastName: String) {
        userRepository.findAll()
    }

    override fun getUsersByName(firstName: String) {
        this.findAll().filter { user: User ->
            user.firstName.contains(firstName)
       }
    }

    override fun getUsersBySurname(lastName: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun findByLogin(login: String): User? {
        return userRepository.findByLogin(login)
    }

    fun <S : User?> save(entity: S): S {
        return userRepository.save(entity)
    }

    fun findAll(): MutableIterable<User> {
        return userRepository.findAll()
    }

    fun deleteAll(entities: MutableIterable<User>) {
        userRepository.deleteAll(entities)
    }

    fun deleteAll() {
        userRepository.deleteAll()
    }

    fun <S : User?> saveAll(entities: MutableIterable<S>): MutableIterable<S> {
        return userRepository.saveAll(entities)
    }

    fun count(): Long {
        return userRepository.count()
    }

    fun findAllById(ids: MutableIterable<Long>): MutableIterable<User> {
        return userRepository.findAllById(ids)
    }

    fun delete(entity: User) {
        userRepository.delete(entity)
    }

    fun findById(id: Long): Optional<User> {
        return userRepository.findById(id)
    }

    fun existsById(id: Long): Boolean {
        return userRepository.existsById(id)
    }

    fun deleteById(id: Long) {
        userRepository.deleteById(id)
    }
}