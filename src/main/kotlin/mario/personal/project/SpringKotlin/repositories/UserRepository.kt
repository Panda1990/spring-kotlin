package mario.personal.project.SpringKotlin.repositories

import mario.personal.project.SpringKotlin.persistence.User
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User, Long> {
    fun findByLogin (login: String): User?
}