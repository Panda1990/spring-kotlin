package mario.personal.project.SpringKotlin.repositories.service

interface UserService {
    fun getUserByNameAndSurname (firstName: String, lastName: String)
    fun getUsersByName (firstName: String)
    fun getUsersBySurname (lastName: String)
}