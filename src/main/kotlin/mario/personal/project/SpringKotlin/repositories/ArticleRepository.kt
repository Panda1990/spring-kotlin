package mario.personal.project.SpringKotlin.repositories

import mario.personal.project.SpringKotlin.persistence.Article
import org.springframework.data.repository.CrudRepository

interface ArticleRepository : CrudRepository <Article, Long> {
    fun findBySlug (slug: String) : Article?
    fun findAllByOrderByAddedAtDesc (): Iterable<Article>
}